# Repository for one-day Akka Course.

## Assignment 1: The Basics
Checkout branch `assignment-01`

## Assignment 2: Supervision
Checkout branch `assignment-02`

## Assignment 3: Testing
Checkout branch `assignment-03`

## Assignment 4: Ask and Futures
Checkout branch `assignment-04`
